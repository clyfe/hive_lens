import time, re, nltk, spear, rfc822

import api


URL_EXPRESSION = re.compile(r'https?://[\w!#$%&\'()*+,./:;=?@\[\]-]+(?<![!,.?;:"\'()-])')


class Result:
    """result from a ranking list"""

    def __init__(self, user, tags, words):
        """creates a new result
        
        :param user: (user, score) tuple returned by spear, score is subunit number
        :type  user: tuple
        :param tags: list of extracted tags
        :type  tags: list
        :param words: list of computed words
        :type  words: list
        """
        self.user = user[1]
        self.score = user[0]
        self.tags = tags
        self.words = words
        
    def percent_score(self):
        """returns the score in percents
        
        :returns: two decimals formatted percent score
        :rtype: string
        """
        return "%.2f" % (self.score * 100)

    def relative_percent_score(self, best_score):
        """returns the score in percents relative to the best score computed
        
        :param best_score: the best score computed
        :type best_score: float
        :returns: two decimals formatted relative percent score
        :rtype: string
        """
        return "%.2f" % (self.score / best_score * 100)



class Analyzer:
    """computes users ranks based on input stream"""
    

    def __init__(self, stream):
        """creates an analyzer from a stream"""
        self.stream = stream
        
        self.contents = []
        self.mentions = []
        for mention in self.stream:
            content = mention['text']
            try:
                content.decode('ascii')
            except UnicodeEncodeError: # TODO: fix unicodes
                continue
            self.mentions.append(mention)
            self.contents.append(content)
        
        self.text_collection = nltk.text.TextCollection(self.contents) # used for tf-idf
        self.tags = {} # extracted tags
        self.words = {} # computed "topics" words via tf-idf
        self._rank() # run the ranking algorithm


    def results(self):
        """returns the computed results and the best score
        
        :returns: results `rank.Result` and best score
        :rtype: 2-tuple
        """
    
        results =[]
        best_score = 0
        for user in self.users:
            result = Result(user, self.tags.get(user[1]), self.words.get(user[1]))
            results.append(result)
            if best_score < result.score: best_score = result.score
        return results, best_score


    def _rank(self):
        """runs the ranking algorithm"""

        activities = []
        for mention in self.mentions:
            content = mention['text']
            author = mention['from_user']

            self._maybe_add_tags(content, author)
            try:
                self._maybe_add_words(content, author)
            except UnicodeEncodeError: # TODO: fix unicodes
                pass
            
            for url in URL_EXPRESSION.findall(content):
                activities.append((
                    rfc822.parsedate(mention['created_at']), # date
                    mention['from_user'], # user
                    api.expand(url) # url
                ))
        
        self.users, self.docs = spear.Spear(activities).run(verbose = False)

    
    def _maybe_add_tags(self, content, author):
        """adds tags if any"""

        tags = re.findall(r"#(\w+)", content)
        if tags:
            if self.tags.get(author):
                self.tags[author] += tags
            else:
                self.tags[author] = tags


    def _maybe_add_words(self, content, author):
        """adds topics if any"""
    
        content_no_links = re.sub(URL_EXPRESSION, '', content)
        tokens = nltk.wordpunct_tokenize(content_no_links)
        words = [w.lower() for w in nltk.Text(tokens)]
                        
        significant_words = []
        for word in words:
            w = word.strip()
            if (len(w) > 2 # insignificant words
                and w not in nltk.corpus.stopwords.words('english') # insignificant words lex
                and re.match("\w", w) # alnum, no symbols, links residue
                and w not in ['http']): # links residue TODO: maybe remove since content_no_links ?
                significant_words.append(w)
        
        ranked_words = [(self.text_collection.tf_idf(term, significant_words), term) for term in significant_words]
        rank_sorted_words = sorted(ranked_words, key = lambda element: element[0])
        words = map(lambda element: element[1], rank_sorted_words[0:4])
        if words:
            if self.words.get(author):
                self.words[author] += words
            else:
                self.words[author] = words

