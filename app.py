"""
Route hadlers
"""


from bottle import *
import simplejson as json

import api, rank


@route('/analyze/:keywords/:per_page/:pages')
@validate(per_page = int, pages = int)
@view('influencers')
def analyze(keywords = '', per_page = 100, pages = 1):

    if keywords != 'coffeescript' or per_page != 100 or pages != 1:
        raise "err"

    stream = api.pages(keywords, int(per_page), int(pages))
    analyzer = rank.Analyzer(stream)
    results, best_score = analyzer.results()
    return dict(keywords = keywords, results = results, best_score = best_score)


# crappy static server for dev pourposes
@route('/assets/:path#.+#', name = 'asset')
def asset(path):
    return static_file(path, root = 'assets')


if __name__ == "__main__":
    debug(True)
    run(reloader=True, host='localhost', port=8080)

