<!DOCTYPE html> 
<html>
    <head>
        <title>HiveLens</title>
                
        <link rel="stylesheet/less" href="/assets/less/bootstrap/bootstrap.less">
        <link rel="stylesheet/less" href="/assets/less/app.less">

        <script src="/assets/js/jquery-1.6.2.min.js"></script>
        <script src="/assets/js/coffee-script.js"></script>
        <script src="/assets/js/less-1.1.4.min.js"></script>        
    </head>
    <body>
    
        <div class="topbar-wrapper" style="z-index: 5;">
            <div class="topbar">
                <div class="topbar-inner">
                    <div class="container">
                        <h3><a href="#">HiveLens</a></h3>
                        <form action="">
                            <input type="text" placeholder="Search" value="{{keywords}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
        %include
        
    </body>
</html>

