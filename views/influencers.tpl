%rebase layout keywords = keywords


<div class="container">
    <div class="content">
    
        <table class="zebra-striped">
            <tr>
                <th>#</th>
                <th>User URL</a></th>
                <th>Tags</th>
                <th>Topics</th>
                <th>Score</th>
                <th>Relative score</th>
                <th>Graph</th>
            </tr>
            %i = 0
            %for result in results:
                <tr>
                    <td>{{i}}</td>
                    <td><a href="http://www.twitter.com/{{result.user}}">{{result.user}}</a></td>
                    <td>
                        %if result.tags:
                            %for tag in set(result.tags):
                                {{tag}}
                            %end
                        %end
                    </td>
                    <td>
                        %if result.words:
                            %for word in set(result.words):
                                {{word}}
                            %end
                        %end
                    </td>
                    <td>{{result.percent_score()}}%</td>
                    %relative_score = result.relative_percent_score(best_score)
                    <td>{{relative_score}}%</td>
                    <td>
                        <div class="rank-graph">
                            <div class="graph-bar" style="width: {{relative_score}}%"></div>
                        </div
                    </td>
                </tr>
                %i += 1
            %end
        </table>
        
    </div>
</div>

