"""
API calls module
Makes api calls to UberVU and URL shortener
"""

import simplejson as json
import urllib2, time

import settings


def pages(keywords, per_page = 100, pages = 1):
    """return a stream of mentions
    
    :param keywords: the keywords that must be mentioned
    :type  keywords: string
    :param per_page: the no of items per page
    :type  per_page: integer or None
    :param pages: the number of pages to be returned
    :type  pages: integer or None
    :returns: last per_page * pages mentions containing keywords
    :rtype: json
    """

    stream = []
    until = None
    params = {'q': keywords, 'rpp': per_page, 'lang': 'en'}

    for page in range(1, pages + 1):
        params['page'] = page
        mentions = twitter(**params)
        results = mentions['results'] # memoize
        if len(results) == 0: break
        stream += results
            
    return stream


def twitter(**params):
    """calls Twitter API with params
    
    :param params: dictinary of key: value params
    :type params: dict
    :returns: JSON stream of results
    :rtype: json
    """

    url = settings.TWITTER_API_BASE + '?' + "&".join('%s=%s' % (k, v) for k, v in params.items())
    return fetch_json(url)


def expand(short):
    """unshors the given URL
    
    :param short: URL to be unshortened
    :type short: string
    :returns: expanded URL
    :rtype: string
    """

    url = 'http://api.longurl.org/v2/expand?&user-agent=Analyzer%2F0.0.1&format=json&url='+short
    result = fetch_json(url)
    return result['long-url']


CACHE = {} # crappy caching for dev pourposes
def fetch_json(url, data=None, timeout=10, retries=3):
    """make API calls and parse JSON results
    
    :param url: URL to make the call to
    :type  url: string
    :param data: extra GET params
    :type  data: string
    :param timeout: time to wait untill fail
    :type  timeout: integer
    :param retries: no of retries untill accept fail
    :type  retries: integer
    :returns: parsed JSON results
    :rtype: json
    """

    try:
        return CACHE[url]
    except KeyError:
        pass
    
    while retries >= 0:
        # Fetch url.
        handle = None
        last_error = None
        try:
            if not data:
                handle = urllib2.urlopen(url, timeout=timeout)
            else:
                data = urllib.urlencode(data)
                handle = urllib2.urlopen(url, data=data, timeout=timeout)
            content = handle.read()
            if content is not None:
                j = json.loads(content)
                CACHE[url] = j
                return j
        except Exception, e:
            last_error = e
        finally:
            # Close the connection
            if handle is not None:
                handle.close()

        # No response from server. Sleep and try again.
        retries -= 1
        if retries >= 0: time.sleep(1)
        
        # Raise error caught from server.
        if last_error: raise last_error # The server failed to respond multiple times.

    return None

