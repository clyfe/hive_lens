import unittest

import rank


class TestAnalyzer(unittest.TestCase):


    def setUp(self):
    
        # number suffix to word makes it unique
        self.stream = [
            {
                'from_user': 'starter',
                'text': 'one two three four five1 http://www.link1.com', # same link
                'created_at': "Wed, 14 Sep 2011 12:47:44 +0000" # higher
            },
            {
                'from_user': 'follower',
                'text': 'one two three four five2 http://www.link1.com', # same link
                'created_at': "Wed, 14 Sep 2011 12:47:45 +0000" # lower
            },
            {
                'from_user': 'wannabe', # soloist
                'text': 'one3 two3 three3 four3 five3 #tag http://www.link2.com',
                'created_at': "Wed, 14 Sep 2011 12:47:42 +0000"
            }
        ]
        
        self.analyzer = rank.Analyzer(self.stream)


    def test_rank(self):
        results, best_score = self.analyzer.results()
        starter, follower, wannabe = results
        stream = self.stream # memoize
        
        self.assertEqual(starter.user, stream[0]['from_user'])
        self.assertEqual(follower.user, stream[1]['from_user'])
        self.assertEqual(wannabe.user, stream[2]['from_user'])
        
        self.assertTrue(starter.score > follower.score > wannabe.score)
        self.assertEqual(starter.score, best_score)
        
        expected = ['one', 'two', 'three', 'four']
        self.assertEqual(starter.words, expected)
        self.assertEqual(follower.words, expected)
        self.assertEqual(wannabe.words, ['one3', 'two3', 'three3', 'four3'])
        
        self.assertEqual(wannabe.tags, ['tag'])


if __name__ == '__main__':
    unittest.main()

